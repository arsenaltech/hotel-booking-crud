<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\HotelRequest;
use App\Hotel;
use App\Image;
use App\Interfaces\AmenityRepositoryInterface;
use App\Interfaces\HotelRepositoryInterface;
use App\Interfaces\ImageRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Session;

/**
 * Class HotelController
 * @package App\Http\Controllers
 */
class HotelController extends Controller
{
    /**
     * @var HotelRepositoryInterface
     */
    protected $hotelRepository;
    /**
     * @var AmenityRepositoryInterface
     */
    protected $amenitiesRepository;
    /**
     * @var ImageRepositoryInterface
     */
    protected $imageRepository;

    /**
     * @param HotelRepositoryInterface $hotelListRepository
     * @param AmenityRepositoryInterface $amenities
     * @param ImageRepositoryInterface $images
     */
    public function __construct(HotelRepositoryInterface $hotelListRepository, AmenityRepositoryInterface $amenities, ImageRepositoryInterface $images) {
        $this->hotelRepository     = $hotelListRepository;
        $this->amenitiesRepository = $amenities;
        $this->imageRepository     = $images;
    }


    /**
     * List all hotels
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $hotels = $this->hotelRepository->getAll($request->get('per_page'), $request->get('page'));
        return response()->json($hotels);
    }

    /**
     * Show the form for creating a new hotel.
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('hotels.create');
    }

    /**
     * Store a newly created hotel.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(HotelRequest $request)
    {
        $responseData = $this->hotelRepository->store($request->all());
        if($request->hasFile('images')) {
            $imageTitleArray = explode(',',$request->get('title'));
            foreach($request->file('images') as $index=>$image){
                $imageName = time() . "_" . $image->getClientOriginalName();
                $path = public_path() .Image::IMAGE_PATH;
                if (!is_dir($path)) {
                    File::makeDirectory($path, 0777, true);
                    chmod($path, 0777);
                }
                if ($image->move($path, $imageName)) {
                    $this->imageRepository->store($responseData->id, array('name' => $imageName, 'path' => Image::IMAGE_PATH, 'title'=>$imageTitleArray[$index]));
                }
            }
        }
        return response()->json(['status'=>true,'message'=>'Hotel added successfully!!']);
    }

    /**
     * Display the hotel information.
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        try{
            $hotelInfo =  $this->hotelRepository->getByID($id);
            if(!empty($hotelInfo))
            {
                return response()->json($hotelInfo);
            }
            else{
                return response()->json(['status'=>false,'message'=>"Invalid Request Id! No Hotel with that ID found in the database!"]);
            }

        } catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }

    /**
     * Update the specified hotel.
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, HotelRequest $request)
    {
        $requestData = $request->all();
        if($this->hotelRepository->update($id,$requestData))
        {
            if($request->hasFile('images')) {
                $imageTitleArray = explode(',',$request->get('title'));
                foreach($request->file('images') as $index => $image){
                    $imageName = time() . "_" . $image->getClientOriginalName();
                    $path = public_path() .Image::IMAGE_PATH;
                    if (!is_dir($path)) {
                        File::makeDirectory($path, 0777, true);
                        chmod($path, 0777);
                    }
                    if ($image->move($path, $imageName)) {
                        $this->imageRepository->store($id, array('name' => $imageName, 'path' => Image::IMAGE_PATH, 'title'=>$imageTitleArray[$index]));
                    }
                }
            }
            if($request->has('image_id')){
                $this->imageRepository->updateImageTitle($requestData['image_id'], $requestData['image_title']);
            }
            return response()->json(["status"=>true,"message"=> "Hotel Information updated successfully!!"]);
        }
        return response()->json(["status"=>false,"message"=> "Something went wrong"]);
    }

    /**
     * Remove the specified hotel.
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $hotelInfo = $this->hotelRepository->getByID($id);
        if($this->hotelRepository->destroy($id))
        {
            foreach($hotelInfo['images'] as $imageArray){
                $this->imageRepository->destroy($imageArray->id);
                File::delete(public_path().Image::IMAGE_PATH.'/'.$imageArray->name);
            }
            return response()->json(["status"=>true,"message"=> "Hotel Information Deleted Successfully."]);
        }
        return response()->json(["status"=>false,"message"=> "Invalid Deletion Request!"]);
    }


    /**
     * Get all Amenities
     * @return mixed
     */
    public function getAmenities()
    {
        $amenities = $this->amenitiesRepository->getAll();
        return response()->json($amenities);

    }


    /**
     * Delete a hotel image.
     * @param $id
     * @return mixed
     */
    public function deleteImage($id)
    {
        $image = $this->imageRepository->find($id);
        if($this->imageRepository->destroy($id))
        {
            File::delete(public_path().Image::IMAGE_PATH.'/'.$image->name);
            return response()->json(["status"=>true,"message"=> "Hotel Image Deleted Successfully."]);
        }
        return response()->json(["status"=>true,"message"=> "Invalid Deletion Request!"]);
    }
}