<?php
/**
 * Created by PhpStorm.
 * User: anil-kumar
 * Date: 27/1/17
 * Time: 3:02 PM
 */

namespace App\Repositories;


use App\Amenity;
use App\Interfaces\AmenityRepositoryInterface;

class AmenityRepository implements AmenityRepositoryInterface
{

    /**
     * Get all Amenity
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return Amenity::all();

    }

}