<?php

namespace App\Repositories;

use App\Image;
use App\Interfaces\AmenityRepositoryInterface;
use App\Interfaces\HotelRepositoryInterface;
use App\Hotel;


class HotelRepository implements HotelRepositoryInterface
{

    /**
     * Fetch hotel according to selected page no.
     * @param $perPage
     * @param $page
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAll($perPage, $page) {
        return Hotel::with('amenities')->paginate($perPage, ['*'], 'page', $page);
    }


    /**
     * Save hotel and their amenity
     * @param $requestData
     * @return static
     */
    public function store($requestData){
        $hotel = Hotel::create($requestData);
        if($requestData['amenities'] != '') {
            $selectedAminity = explode(',', $requestData['amenities']);
            foreach ($selectedAminity as $index => $amenityId) {
                $hotel->amenities()->attach($amenityId);
            }
        }
        return $hotel;
    }


    /**
     * Select hotel information by id.
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function getByID($id){
        return Hotel::with('amenities','images')->findOrFail($id);
    }


    /**
     * Update hotel information and their amenity
     * @param $id
     * @param $requestData
     * @return bool
     */
    public function update($id,$requestData){
        $hotelsList = $this->getByID($id);
        $hotel = $hotelsList->update($requestData);
        $hotelsList->amenities()->detach();
        if($requestData['amenities'] != '') {
            $selectedAminity = explode(',', $requestData['amenities']);
            foreach ($selectedAminity as $index => $amenityId) {
                $hotelsList->amenities()->attach($amenityId);
            }
        }
        return $hotel;

    }


    /**
     * Delete selected hotel information
     * @param $id
     * @return int
     */
    public function destroy($id){
        return Hotel::destroy($id);
    }

}
