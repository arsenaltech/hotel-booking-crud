<?php
/**
 * Created by PhpStorm.
 * User: anil-kumar
 * Date: 27/1/17
 * Time: 3:02 PM
 */

namespace App\Repositories;


use App\Hotel;
use App\Image;
use App\Interfaces\ImageRepositoryInterface;

class ImageRepository implements ImageRepositoryInterface
{

    /**
     * Get selected image info.
     * @param $id
     * @return mixed
     */
    public function find($id){
        return Image::find($id);
    }


    /**
     * Save hotel image
     * @param $hotelId
     * @param $requestData
     */
    public function store($hotelId, $requestData)
    {
        $hotel = Hotel::find($hotelId);
        $image = new Image();
        $image->name = $requestData['name'];
        $image->path = $requestData['path'];
        $image->title = $requestData['title'];
        $image->save();
        $hotel->images()->attach($image->id);
    }


    /**
     * Update the image title.
     * @param $imageId
     * @param $title
     */
    public function updateImageTitle($imageId, $title)
    {
        $imageIdArray = explode(',',$imageId);
        $imageTitleArray = explode(',',$title);
        foreach($imageIdArray as $key=>$id){
            Image::where('id',$id)->update(['title'=>$imageTitleArray[$key]]);
        }

    }


    /**
     * Delete the selected images.
     * @param $id
     * @return int
     */
    public function destroy($id)
    {
        return Image::destroy($id);
    }

}